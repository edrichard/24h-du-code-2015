<?php
require_once('class/Database.php');
require_once('class/Arret.php');
require_once('class/AssocAL.php');
require_once('class/Ligne.php');

function beginGame($idDepart, $idArrivee, $dateDe){

	$heureActuelle = $dateDe->format('H:i:s'); 

	switch ($dateDe->format("N")) 
	{
		case 6:
				$jourActuel = "sa";
			break;
		case 7:
				$jourActuel = "di";
			break;
		default:
				$jourActuel = "se";
			break;
	}
	$listArret = array();
	$listArret[] = $idDepart;
	
	$db = Database::getInstance();
	$sql = 'SELECT ligne_id FROM `assocal` WHERE `arret_id` = '.$idDepart;
	$query = $db->prepare($sql);
	$query->execute();
	$ligneListe = $query->fetchAll(PDO::FETCH_ASSOC);
	$results = array();
	
	$sql = $sql = 'SELECT ligne_id FROM `assocal` WHERE `arret_id` = '.$idArrivee;
	$query1 = $db->prepare($sql);
	$query1->execute();
	$listeLigneArrivee = $query1->fetchAll(PDO::FETCH_ASSOC);
	var_dump($listeLigneArrivee);
	
	foreach ($ligneListe as $ligne) {
		$results = getListAssocAL(array(), $listeLigneArrivee, $ligne["ligne_id"], 0, array(), $idArrivee, $heureActuelle, $jourActuel);
	}
	
	$solution = $results[0];
	foreach ($results as $result) {
		if(count($solution) > count($result))
		{
			$solution = $result;
		}
	}
	
	foreach ($solution as $arret) {
		echo"<p>R : ".$arret["idArret"]." ".$arret["time"]."</p>";
	}
}

beginGame("1334", "1282", new DateTime());

/**
 * retourne liste des arrets qui entre l'arret de départ et de fin
 */
function getListAssocAL($listAssoALs, $listLignesCommunArrivee, $ligneActuelle, $poidsCumule, $listLignesDejaFaites, $idArretFinal, $heureActuelle, $jourActuel){
	
	$continuer = true;
	$listDicLignesCommunes = getLignesCommunes($listAssoALs[count($listAssoALs)-1], $ligneActuelle, $heureActuelle, $jourActuel);

	$result = array();
	$poidsMin = null;
	if(count($listDicLignesCommunes) > 0){
		foreach ($listDicLignesCommunes as $dicLigneCommune) {
			if(!in_array($dicLigneCommune["ligne_id"], $listLignesDejaFaites)){
				$dictionnaire = array();
				$dictionnaire["idArret"] = $dicLigneCommune["arret_id"];
				$dictionnaire["time"] = $dicLigneCommune["heureDepart"];
				$listAssoALs[] = $dictionnaire;//AssocAL::read($dicLigneCommune["arret"]->getArretId(),$dicLigneCommune["ligne"]->getLigneId());
				$listLignesDejaFaites[] = $dicLigneCommune["ligne_id"];
				if(in_array($dicLigneCommune["ligne_id"], $listLignesCommunArrivee))
				{
					$poidsTmp = $poidsCumule + $dicLigneCommune["poids"] + getPoidsEntreDeuxArretsSurUneMemeLigne(AssocAL::read($dicLigneCommune["arret"], $dicLigneCommune["ligne_id"]), AssocAL::read($idArretFinal, $dicLigneCommune["ligne_id"]));
					if(is_null($poidsMin) || $poidsMin > $poidsTmp)
					{
						$continuer = false;
						$result = $listAssoALs;
						$poidsMin = $poidsTmp;
					}
				}else{
					if($continuer){
						$result = getListAssocAL($listAssoALs, $listLignesCommunArrivee, $dicLigneCommune["ligne_id"], $poidsCumule, $listLignesDejaFaites, $idArretFinal, $dicLigneCommune["heureDepart"],$jourActuel);	
					}		
				}
			}
		}
	}
	
	return $result;
}


/**
 * retourne une liste de dictionnaire composé de 4 elements (Arret, poids, Ligne, Heure) correspondant au lignes qui croise la ligne courante suivant l'heure actuelle
 * retourne listArray vide si le résultat est négatif
 */
function getLignesCommunes($arret, $ligneCourante, $heure, $jour){
		
		error_log("heure :: ".$heure);
		error_log("jour :: ".$jour);
	

	$db = Database::getInstance();
	$sql = 'SELECT * FROM `assocal` WHERE `ligne_id` = '.$ligneCourante.' AND `arret_id` = '.$arret;
	$query0 = $db->prepare($sql);
	$query0->execute();
	$assocalDepart = $query0->fetch(PDO::FETCH_ASSOC);
	
	$query = $db->prepare('
		select
		    assocal.assocal_id,
		    ligne.ligne_nom,
		    assocal.ligne_id,
		    assocal.arret_id,
		    (assocal_position - (select assocal_position from assocal where arret_id = :arret_id and ligne_id = :ligne_id)) as diff_position
		from assocal
		join ligne on ligne.ligne_id = assocal.ligne_id
		
		where arret_id in(
		    select arret_id
		    from assocal
		    where ligne_id = :ligne_id
		)
		and ligne_nom <> (select concat(substr(ligne_nom, 1, LENGTH(ligne_nom)-1), "A") as ligne_nom from ligne where ligne_id = :ligne_id)
		and ligne_nom <> (select concat(substr(ligne_nom, 1, LENGTH(ligne_nom)-1), "R") as ligne_nom from ligne where ligne_id = :ligne_id);
	');
	$query->execute(array('arret_id' => $arret, 'ligne_id' => $ligneCourante));
	$tmpResult = $query->fetchAll(PDO::FETCH_ASSOC);
	$results = array();
	
	foreach($tmpResult as $key => $result) {
		
		$delete = false;
		if($result["diff_position"] < 0)
		{
			$result["ligne_nom"] = substr($result["ligne_nom"], -1) == "R" ?  substr($result["ligne_nom"], 0, -1)."A" : substr($result["ligne_nom"], 0, -1)."R";
			$sql = 'SELECT * FROM `assocal` WHERE `ligne_id` = (select ligne_id from ligne where ligne_nom = "'.$result["ligne_nom"].'") AND `arret_id` = '.$arret;
			$queryA = $db->prepare($sql);
			$queryA->execute();
			$nouvelleLigne = $queryA->fetch(PDO::FETCH_ASSOC);
			
			$result["assocal_id"] = $nouvelleLigne["assocal_id"];
			$result["diff_position"] = abs(intval($result["diff_position"]));
			
			if($nouvelleLigne == false) {
				$delete = true;
			}
			
		}
		$sql = 'select * from heure_'.$result["assocal_id"].' where heure_position = (select heure_position from heure_'.$assocalDepart["assocal_id"].' where heure_horaire > "'.$heure.'" and heure_jour = "'.$jour.'" limit 1) and heure_jour = "'.$jour.'" limit 1';

		$query2 = $db->prepare($sql);
		$query2->execute();
		$heureDepart = $query2->fetch(PDO::FETCH_ASSOC);
		$result["heureDepart"] = $heureDepart['heure_horaire'];
		if(!$delete) {
			
			$results[] = $result;
		}
		
	}
	return $results;
}

//getLignesCommunes(1113, 36, "10:00:00", "se");

/**
 * calcule le poids entre deux arrets par rapport à leurs positions
 */
 function getPoidsEntreDeuxArretsSurUneMemeLigne($assocALDepart, $assocALArrivee){
 	$result = 0;
	
 	if($assocALDepart->assocal_position > $assocALArrivee->assocal_position){
 		$result = $assocALDepart->assocal_position - $assocALArrivee->assocal_position;
 	}else{
 		$result = $assocALArrivee->assocal_position - $assocALDepart->assocal_position;
 	}
	
	return $result;
 }

?>