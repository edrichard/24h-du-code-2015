<?php
set_time_limit(0);

require("../entity/Arret.php");
require("../entity/Ligne.php");
require("../entity/Assocal.php");
require("../entity/Heure.php");

$db2 =  new DB();
$db2_pdo = $db2->getPdo();

$arret = new Arret();
$ligne = new Ligne();
$assocArretLigne = new Assocal();
$heure = new Heure();

$query_create_table_arret = $db2_pdo->query('CREATE TABLE IF NOT EXISTS arret (arret_id INT NOT NULL , arret_nom VARCHAR(250) NOT NULL ,PRIMARY KEY  (arret_id)) ENGINE = InnoDB;');
$query_create_table_arret->execute();

$query_create_table_ligne = $db2_pdo->query('CREATE TABLE IF NOT EXISTS ligne (ligne_id INT NOT NULL PRIMARY KEY , ligne_nom VARCHAR(250) NOT NULL , arret_dest_id INT NOT NULL , ligne_nb_arret INT NOT NULL) ENGINE = InnoDB;');
$query_create_table_ligne->execute();

$query_create_table_assocal = $db2_pdo->query('CREATE TABLE IF NOT EXISTS  assocal (assocal_id INT NOT NULL AUTO_INCREMENT , arret_id INT NOT NULL , ligne_id INT NOT NULL , assocal_position INT NOT NULL , PRIMARY KEY (assocal_id)) ENGINE = InnoDB;');
$query_create_table_assocal->execute();

$pathDir = "fixtures";
$forbiddenDir = array('.', '..', 'NULL.json');

$files = scandir($pathDir);
$stopArray = array();
foreach($files as $file)
{
    if(!in_array($file, $forbiddenDir)) {
        $json = json_decode(file_get_contents($pathDir.'/'.$file), true);

        $ligne->ligne_nom = $json["track_number"];
        $ligne->ligne_nb_arret = $json["n_stops"];
        $ligne->arret_dest_id = $json["track_dest"]["id"];
        $ligne->ligne_id = intval($json["track_id"]);
        $ligne->create();
		
        foreach($json["stops"] as $stop)
        {
            if(!in_array(intval($stop["id"]), $stopArray)) {
                $stopArray[] = intval($stop["id"]);
                $arret->arret_id = intval($stop["id"]);
                $arret->arret_nom = $stop["name"];
                $arret->create();
            }

            $assocArretLigne->arret_id = intval($stop["id"]);
            $assocArretLigne->ligne_id = $json["track_id"];
            $assocArretLigne->assocal_position = $stop["position"];
            $assocArretLigne->create();
			
			$query_assocal_id = $db2_pdo->query('select assocal_id from assocal where arret_id = '.intval($stop["id"]).' AND ligne_id = '.$json["track_id"]);
			$query_assocal_id->execute();
			$results = $query_assocal_id->fetchAll();
			
			$query_create_table = $db2_pdo->query('CREATE TABLE IF NOT EXISTS heure_'.$results[0]["assocal_id"].'
				(
					`assocal_id` INT NOT NULL,
					`heure_jour` VARCHAR(20) NOT NULL,
					`heure_horaire` TIME NOT NULL,
					`heure_position` INT
				) ENGINE = InnoDB;
			');
			$query_create_table->execute();
			
            $heure->assocal_id = $results[0]["assocal_id"];
			
            foreach($json["schedule"][$stop["id"]] as $day => $schedule)
            {
            	if(in_array($day, array('lu', 'sa', 'di'))) {
            		$zDay = ($day == 'lu') ? 'se' : $day;
					$i = 0;

	                foreach($schedule as $time)
	                {
	                	$i++;
	                	$query_add_heure = $db2_pdo->query('INSERT INTO heure_'.$results[0]["assocal_id"].' VALUES('.intval($results[0]["assocal_id"]).', "'.$zDay.'", "'.$time.'", '.$i.')');
						$query_add_heure->execute();
					}
            	}
            }
        }
    }
}
