<?php

class Database extends PDO
{

	/**
	 * Instance courante.
	 */
	private static $instance = null;

	/**
	 * Nouvelle connexion PDO.
	 */
	public function __construct()
	{
		parent::__construct('mysql:charset=UTF8;dbname=urbanflow;host=localhost', 'root', 'hmdnx29');
	}

	/**
	 * Génère et obtient l'instance de la base de données.
	 *
	 * @return PDO Instance de la base.
	 */
	public static function getInstance()
	{
		if(is_null(self::$instance))
		{
			self::$instance = new Database();
		}

		return self::$instance;
	}

}

?>
