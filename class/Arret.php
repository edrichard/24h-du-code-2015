<?php

class Arret {
	
	/**
	 * privates properties
	 */
	private $arret_id;
	private $arret_name;
	
	//constructeur
	public function __construct($id, $name){
		$this->arret_id = $id;
		$this->arret_name = $name;
	}
	
	/**
	 * getters
	 */
	 
	 /**
	  * retourne l'id de l'arret
	  */
	  function getArretId(){
	  	return $this->arret_id;
	  }
	 
	/**
	 * retourne le nom de l'arret
	 */
	 function getArretNom(){
	 	return $this->arret_name;
	 }
	 
	/**
	 * setters
	 */
	
	/**
	 * definit l'id de l'arret
	 */
	 function setArretId($id){
	 	$this->arret_id = $id;
	 }
	 
	/**
	 * definit l'id de l'arret
	 */
	 function setArretNom($nom){
	 	$this->arret_name = $nom;
	 }
	 
	 
	/**
	 * retourne l'arret correspondant à l'id_arret sinn retourne null
	 */
	public static function read($id_arret){
		$db = Database::getInstance();
		$query = $db->prepare("Select arret_id, arret_nom from arret where arret_id = ?");
		$query->execute(array($id_arret));
		return $query->fecthAll(PDO::FETCH_ASSOC);
	}
	
	
}

?>