<?php

class Ligne {
	
	/**
	 * privates properties
	 */
	 private $ligne_id;
	 private $ligne_nom;
	 private $arret_dest;
	 private $ligne_nb_arret;
	 
	 
	 /**
	  * Constructeur
	  */
	  public function __construct($id, $nom, $arret_dest_id, $ligne_nb_arret){
	  	$this->ligne_id = $id;
		$this->ligne_nom = $nom;
		$this->arret_dest = $arret_dest;
		$this->ligne_nb_arret = $ligne_nb_arret;
	  }
	  
	 /**
	  * getters
	  */
	  
	  /**
	   * retourne l'id de la ligne
	   */
	  function getLigneId(){
	  	return $this->ligne_id;
	  }
	  
	  /**
	   * retourne le nom de la ligne
	   */
	  function getLigneNom(){
	  	return $this->ligne_nom;
	  }
	  
	  /**
	   * retourne l'id de l'arret dest de la ligne
	   */
	  function getArretDest(){
	  	return $this->arret_dest;
	  }
	  
	  /**
	   * retourne le nb d'arret sur la ligne
	   */
	  function getLigneNbArret(){
	  	return $this->ligne_nb_arret;
	  }
	  
	  /**
	   * setters
	   */
	   
	  /**
	   * definit l'id de la ligne
	   */
	  function setLigneId($ligne_id){
	  	$this->ligne_id = $ligne_id;
	  }
	  
	  /**
	    * definit le nom de la ligne
	    */
	  function setLigneNom($ligne_nom){
	  	$this->$ligne_nom = $ligne_nom;
	  }
	  
	  /**
	    * definit le nom de la ligne
	    */
	  function setArretDest($arret_dest){
	  	$this->arret_dest = $arret_dest;
	  }
	  
	  /**
	    * definit le nb de d'arret de la ligne
	    */
	  function setLigneNbArret($ligne_nb_arret){
	  	$this->ligne_nb_arret = $ligne_nb_arret;
	  }
	  
	  
	 /**
	  * retourne la ligne correspondant à l'id_ligne sinn retourne null
	  */
	public static function read($ligne_id){
		$db = Database::getInstance();
		$query = $db->prepare("Select ligne_id, ligne_nom, arret_dest_id, ligne_nb_arret from ligne where ligne_id = ?");
		$query->execute(array($ligne_id));
		$result = $query->fecthAll(PDO::FETCH_ASSOC);
		return Ligne($result->ligne_id, $result->ligne_nom, Arret::read($result->arret_dest_id), $result->ligne_nb_arret);
	}
}

?>