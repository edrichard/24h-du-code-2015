<?php

class AssocAL {
	
	/**
	 * privates properties
	 */
	 private $assocal_id;
	 private $arret;
	 private $ligne;
	 private $assocal_position;
	 
	 /**
	  * Constructeur
	  */
	  public function __construct($id, $arret, $ligne, $assocal_postion){
	  	$this->assocal_id = $id;
		$this->arret = $arret;
		$this->ligne = $ligne;
		$this->assocal_position = $assocal_postion;
		
	  }
	  
	  /**
	   * getters
	   */
	   
	   /**
	    * retourne l'id de l'assocAL
	    */
	    function getAssocALId(){
	    	return $this->assocal_id;
	    }
		
	   /**
	    * retourne l'arret du assocAL
	    */
	    function getArret(){
	    	return $this->arret;
	    }
		
	   /**
	    * retourne la ligne du assocAL
	    */
	    function getLigne(){
	    	return $this->ligne;
	    }
		
	   /**
	    * retourne la postion sur la ligne de l'assocAL
	    */
	    function getAssocALPosition(){
	    	return $this->assocal_position;
	    }
		
	  /**
	   * setters
	   */
	   
	   /**
	    * definit l'id de l'assocAL
	    */
	    function setAssocALId($id){
	    	$this->assocal_id = $id;
	    }
		
	  /**
	    * definit l'arret de l'assocAL
	    */
	    function setArret($arret){
	    	$this->arret = $arret;
	    }
		
	  /**
	    * definit la ligne de l'assocAL
	    */
	    function setLigne($ligne){
	    	$this->ligne = $ligne;
	    }
		
	  /**
	    * definit la position de l'assocAL
	    */
	    function setAssocALPosition($assocal_position){
	    	$this->assocal_id = $id;
	    }
		
		
	 /**
	  * retourne l'assocAL correspondant à l'assocAL_id sinn retourne null
	  */
	public static function read($assocAL_id){
		$db = Database::getInstance();
		$query = $db->prepare("Select assocal_id, arret_id, ligne_id, assocal_position from assocal where assocal_id = ?");
		$query->execute(array($assocAL_id));
		$result = $query->fecthAll(PDO::FETCH_ASSOC);
		return AssocAL($result->assocal_id, Arret::read($result->arret_id), Ligne::read($result->ligne_id), $result->assocal_position);
	}
	
	/**
	  * retourne l'assocAL correspondant à l'arret_id et ligne_id sinn retourne null
	  */
	public static function find($arret_id, $ligne_id){
		$db = Database::getInstance();
		$query = $db->prepare("Select assocal_id, arret_id, ligne_id, assocal_position from assocal where arret_id = ? and ligne_id = ?");
		$query->execute(array($arret_id, $ligne_id));
		$result = $query->fecthAll(PDO::FETCH_ASSOC);
		return AssocAL($result->assocal_id, Arret::read($result->arret_id), Ligne::read($result->ligne_id), $result->assocal_position);
	}
	
	/**
	  * retourne l'assocAL correspondant à l'assocAL_id sinn retourne null
	  */
	public static function getTousLesArretsDeLaLigne($ligne_id){
		$db = Database::getInstance();
		$query = $db->prepare("Select assocal_id, arret_id, ligne_id, assocal_position from assocal where ligne_id = ?");
		$query->execute(array($ligne_id));
		$result = $query->fecthAll(PDO::FETCH_ASSOC);
		return AssocAL($result->assocal_id, Arret::read($result->arret_id), Ligne::read($result->ligne_id), $result->assocal_position);
	}
}

?>